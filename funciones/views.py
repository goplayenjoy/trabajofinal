from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import PayForm, DebtForm
from .models import Pay, Debt, DebtPayment
from django.contrib.auth.decorators import login_required

@login_required

# Create your views here.
def add_Fee(request):
	if request.method == 'POST':
		payForm = PayForm(request.POST)
		if payForm.is_valid():
			payForm.save()
		return redirect('payments')
	else:
		payForm = PayForm()
	return render(request,'addPayment.html', {'payForm':payForm})

def Payments(request):
	context = {}
	return render(request,'pagos.html', context)

def Consolidate(request):
	context = {}
	return render(request,'consolidate.html', context)

def Reports(request):
	context = {}
	return render(request,'reports.html', context)

def Debts(request):
	debt = Debt.objects.all()
	context = {'deudas': debt}
	return render(request,'debts.html', context)

def pay_Debt(request):
	Data: {}
	return render(request,'consolidar.html', Data)