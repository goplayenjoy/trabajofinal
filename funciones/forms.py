from django import forms
from .models import Pay, Debt, DebtPayment

class PayForm(forms.ModelForm):
	class Meta:
		model = Pay
		fields = [
		'product',
		'amount',
		'debtor',
        'date',
		]
		widgets = {
		'product': forms.TextInput(attrs={'class':'form-control'}),
		'amount': forms.TextInput(attrs={'class':'form-control'}),
		}


class DebtForm(forms.ModelForm):
	class Meta:
		model = DebtPayment
		fields = [
		'transaction',
		'comentary',
		]
