from django.contrib import admin

# Register your models here.
from .models import Persons, Pay, Debt, DebtPayment

# Register your models here.


class PersonsAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'active',
        )
admin.site.register(Persons, PersonsAdmin)

class PayAdmin(admin.ModelAdmin):
	list_display = (
        'product',
        'amount',
        'debtor',
        )
admin.site.register(Pay, PayAdmin)


class DebtAdmin(admin.ModelAdmin):
	pass
	list_display = (
        'product',
        'amount',
        'status',
        'payer')
admin.site.register(Debt, DebtAdmin)

class DebtPaymentAdmin(admin.ModelAdmin):
    list_display = (
        'transaction',
    )
    pass
admin.site.register(DebtPayment, DebtPaymentAdmin)
