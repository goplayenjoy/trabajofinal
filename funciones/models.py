from django.db import models
from django.contrib.auth.models import User

class Persons(models.Model):
    title = models.CharField(max_length=144)
    content = models.TextField()
    active = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

class Pay (models.Model):
    product = models.CharField(max_length=144)
    amount = models.IntegerField()
    debtor = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
    	return self.product

class Debt (models.Model):
    product = models.CharField(max_length=144)
    amount = models.IntegerField()
    payer = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.BooleanField(default=False)
    
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
    	return self.product

class DebtPayment (models.Model):
	transaction = models.IntegerField()
	comentary = models.TextField()

