from django.urls import path
from . import views

urlpatterns = [
    path('addFee', views.add_Fee, name="addFee"),
    path('debts', views.Debts, name="debts"),
    path('payDebt', views.pay_Debt, name="payDebt"),
    path('payments', views.Payments, name="payments"),
    path('consolidate', views.Consolidate, name="consolidate"),
    path('reports', views.Reports, name="reports"),
]
