from django.contrib import admin
from .models import Cuenta

# Register your models here.
class CuentaAdmin(admin.ModelAdmin):
    list_display = (
        'banco',
        'tipoCuenta',
        'numeroCuenta',
        'run',
        'usuario',
        )
admin.site.register(Cuenta, CuentaAdmin)