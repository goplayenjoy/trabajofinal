from django.shortcuts import render, redirect
from funciones.models import Pay, Debt, Persons, DebtPayment
from .models import Cuenta
from funciones.forms import PayForm, DebtForm
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.db.models import Sum




@login_required(login_url="/auth/login")
def home(request):
    template_name = 'home.html'
    data = {}
    data['payed'] = Pay.objects.aggregate(Sum('amount'))['amount__sum']
    data['debt'] = Debt.objects.aggregate(Sum('amount'))['amount__sum']
    # data['debtPayment'] = 'payed ['amount__sum']
    # select * from blog WHERE status = 1
    # data['posts'] = Persons.objects.filter(active=True) 

    return render(request, template_name, data)

@login_required
def perfil(request):
	template_name = 'perfil.html'
    # usuario=  U.objects.all()
    # cuenta=  Cuenta.objects.all()
    # context={'usuarios':usuario}
	data = {}
	return render(request, template_name, data)

