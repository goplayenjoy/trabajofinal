# Generated by Django 2.1.7 on 2019-06-07 17:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_post_photo'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='status',
            new_name='prominent',
        ),
    ]
