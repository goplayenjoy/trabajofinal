from django.contrib import admin
from news.models import Category, Post

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):

    list_display = (
        'title', 
        'date_published', 
        'prominent', 
        'sort_order', 
        'category', 
    )

